
# VIM Javascript plugin

## FTDetect

Add non-dot-js file detection and node shebang detection.

## Indent

I’ve never been satisfied with any if the javascript indent files that I’ve
tried so I wrote my own. It supports comma-first style.

If you set `g:javascript_double_literal_indent` to 1, you’ll get:

```
#!javascript

{
    foo : 'bar'
  , bar : 'baz'
}

[
    'foo'
  , 'bar'
]
```

Instead of:

```
#!javascript

{
  foo : 'bar'
  , bar : 'baz'
}

[
  'foo'
  , 'bar'
]
```

