"
" Detect NodeJS shebang
"

function! DetectJS()
  if getline(1) =~ '^#.*\<node\>'
    setlocal ft=javascript
  endif
endfunction
au BufNewFile,BufRead * call DetectJS()

"
" Non .js files that are Javascript
"

au BufNewFile,BufRead *.json,Jakefile,Meta.info setlocal ft=javascript
