" Vim Javascript indent file
" Language: JavaScript
" Author: Kevin Faber <kevin.fab@gmail.com>
" Last Change: 2013-10-13
"
" TODO Add ES6 support
"

" Only load this file when no other was loaded.
if exists('b:did_indent')
  finish
endif
let b:did_indent = 1

" Set expression.
setlocal indentexpr=GetJavascriptIndent()

" Chars that trigger indent.
setlocal indentkeys=0{,0},0),0],:,\,,!^F,o,O,e,*<Return>,=*/

" Clean CR when the file is in Unix format.
if &fileformat == "unix"
  silent! %s/\r$//g
endif

" Only define the functions once per session.
if exists("*GetJavascriptIndent")
  finish
endif

" Get decrease amount of indentation based on comment begining.
function! s:GetCommentDecreaseAmount(lnum)
  let lnum = a:lnum

  while lnum > 0
    let lnum = prevnonblank(lnum)
    let line = getline(lnum)
    if line =~ '^\s*/\*\(\*\|\s\=-\|!\)'
      return 2
    elseif line =~ '^\s*/\*'
      return 1
    endif
    let lnum -= 1
  endwhile

  return 0
endfunction

" Check if given string contains a statement keyword
function s:ContainsStatementKeyword(str)
  if a:str =~ '\<catch\>\|else\>\|for\>\|function\>\|if\>\|try\|while\>'
    return 1
  endif

  return 0
endfunction

" Check if given string starts with a statement keyword
function s:StartsWithStatementKeyword(str)
  if a:str =~ '^\s*\(\<catch\>\|else\>\|for\>\|function\>\|if\>\|try\>\|while\>\)'
    return 1
  endif

  return 0
endfunction

" Detect if user wants to double intent of the first item
" after `{`, `[` or `(` + line break
if exists('g:javascript_double_literal_indent') && g:javascript_double_literal_indent == 1
  let s:javascript_double_literal_indent = 1
else
  let s:javascript_double_literal_indent = 0
endif

" Indent detection
function! GetJavascriptIndent()
  let pnum = prevnonblank(v:lnum - 1)
  if pnum == 0
    return 0
  endif
  let line = getline(v:lnum)
  let pline = getline(pnum)
  let bpnum = prevnonblank(pnum - 1)
  let bpline = getline(bpnum)
  let ind = indent(pnum)

  " Do nothing starts and closes a function
  if pline =~ '^\s*function' && pline =~ '}\s*$'
    return ind
  endif

  " Do nothing if line consists of a bracket or parent
  " and previous line ends with a the opposite
  if line =~ '^\s*}' && pline =~ '{\s*$'
    return ind
  elseif line =~ '^\s*)' && pline =~ '(\s*$'
    return ind
  elseif line =~ '^\s*]' && pline =~ '[\s*$'
    return ind
  endif

  if s:javascript_double_literal_indent
    " Increase 2* if previous line ends with a `{`
    " but does not contains a statement keyword.
    if pline =~ '{\s*$' && !s:ContainsStatementKeyword(pline)
      let ind = ind + 2 * &sw
      return ind
    endif

    " Increase 2* if previous line ends with a `(` of `[`.
    if pline =~ '(\s*$\|[\s*$'
      let ind = ind + 2 * &sw
      return ind
    endif
  endif

  " Increase if previous line starts with `var` but does not end with a `;`.
  if pline =~ '^\s*\<var\>[^;]*$'
    let ind = ind + &sw
    return ind
  endif

  " Increase if previous line ends with a `{`, `[` or `(`.
  if pline =~ '{\s*$\|[\s*$\|(\s*$'
    let ind = ind + &sw
    return ind
  endif

  " Increase if previous line starts with a statement keyword
  " unless it ends with a `;` or `{`.
  if s:StartsWithStatementKeyword(pline) && pline !~ ';\s*$\|{\s*$'
    let ind = ind + &sw
    return ind
  endif

  if s:javascript_double_literal_indent
    " Decrease 2* if line consists of a }
    " and previous line does not start with a `,`
    " and the line before ends with `{` but does not contains a statement keyword
    if line =~ '^\s*}' && pline !~ '^\s*,' && bpline =~ '{\s*$' && !s:matchesstatementkeyword(bpline)
      let ind = ind - 2 * &sw
      return ind
    endif

    " Decrease 2* if line consists of a `)` or a `]`
    " and previous line does starts with a `,`
    " and the line before ends with `[` or `(`
    if line =~ '^\s*]\|^\s*)' && pline !~ '^\s*,' && bpline =~ '[\s*$\|(\s*$'
      let ind = ind - 2 * &sw
      return ind
    endif
  endif

  " Decrease if line starts with a `,`
  " but previous line does not start with `,` or `var`.
  if line =~ '^\s*,' && pline !~ '^\s*,' && pline !~ '^\s*var'
    let ind = ind - &sw
    return ind
  endif

  " Decrease if previous line starts with a `,` and ends with a `;`.
  if pline =~ '^\s*,[^;]*;\s*$'
    let ind = ind - &sw
    return ind
  endif

  " Decrease if previous line consists of a `;`.
  if pline =~ '^\s*;'
    let ind = ind - &sw
    return ind
  endif

  " Decrease if line ends with a `;`
  " and the line before the previous ends with a `,`
  if pline =~ ';\s*$' && bpline =~ ',\s*$'
    let ind = ind - &sw
    return ind
  endif

  " Decrease 2* if line ends with a `}`
  " and the line before the previous starts with a statement keyword
  " but does not end with a `;` or `{`
  if line =~ '^\s*}' && s:StartsWithStatementKeyword(bpline) && bpline !~ ';\s*$\|{\s*$'
    let ind = ind - 2 * &sw
    return ind
  endif

  " Decrease if current line consists of a `}`, `]` or `)`.
  if line =~ '^\s*}\|^\s*]\|^\s*)'
    let ind = ind - &sw
    return ind
  endif

  " Decrease if the line before the previous starts with a statement keyword
  " unless it ends with a `;` or `{`.
  if s:StartsWithStatementKeyword(bpline) && bpline !~ ';\s*$\|{\s*$'
    let ind = ind - &sw
    return ind
  endif

  " Add 2 for comments starting with `/**` or `/*-` of `/* -`
  " otherwise add 1.
  if pline =~ '^\s*/\*\(\*\|\s\=-\)'
    let ind = ind + 2
  elseif pline =~ '^\s*/\*'
    let ind = ind + 1
  endif

  " Decrease based on begining of comment
  if pline =~ '\*/$'
    let ind = ind - s:GetCommentDecreaseAmount(pnum)
  endif

  return ind
endfunction
